#!/usr/bin/env sh

############## Important ##############
# do not use double bracket for ifs, it does not work in openjdk:11-jdk sh

export CI_BUILD_BEFORE_SHA
export CI_BUILD_NAME
export CI_BUILD_REF
export CI_BUILD_STAGE

export CI_COMMIT_BRANCH

export CI_MERGE_REQUEST_IID
export CI_MERGE_REQUEST_SOURCE_BRANCH_NAME
export CI_MERGE_REQUEST_TARGET_BRANCH_NAME

export CI_JOB_NAME
export CI_JOB_STAGE

export CI_PIPELINE_SOURCE


echo "********************************************************************************"
echo "CI_PIPELINE_SOURCE:  ${CI_PIPELINE_SOURCE}"
echo "CI_BUILD_NAME:       ${CI_BUILD_NAME}"
echo "CI_BUILD_STAGE:      ${CI_BUILD_STAGE}"
echo "CI_JOB_NAME:         ${CI_JOB_NAME}"
echo "CI_JOB_STAGE:        ${CI_JOB_STAGE}"
echo "***********************************   INFO   ***********************************"

if [ "${CI_PIPELINE_SOURCE}" = "merge_request_event" ]; then
    ############## MR ##############
    echo "Event:    Merge Request"
    echo "MR ID:    ${CI_MERGE_REQUEST_IID}"
    echo "MR from:  ${CI_MERGE_REQUEST_SOURCE_BRANCH_NAME}"
    echo "MR to:    ${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}"
elif [ "${CI_PIPELINE_SOURCE}" = "push" ]; then
    ############## Push ##############
    echo "Event:        Push"
    echo "Branch:       ${CI_COMMIT_BRANCH}"
    echo "This commit:  ${CI_BUILD_REF}"
    echo "Last commit:  ${CI_BUILD_BEFORE_SHA}"
else
    export
fi
