package com.artemkaxboy.test

import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Test

internal class ApplicationKtTest {

    @Test
    fun permanentTest() {
        val fail = false
        assertFalse(fail, "permanent test result was manually changed")
    }

    @Test
    fun flakyTest() {
        val fail = getTrue(25)
        assertFalse(fail, "flaky test met error")
    }
}
