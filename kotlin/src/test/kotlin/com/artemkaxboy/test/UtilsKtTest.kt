package com.artemkaxboy.test

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import kotlin.math.absoluteValue

internal class UtilsKtTest {

    @Test
    fun getTrueFailsToMeetAllowedDeviation() {
        val times = 10_000
        val allowedDeviationPercent = 1.5
        val chance = 10

        val fact = repeat(times) { getTrue(chance) }
            .filter { it }
            .count()

        val deviation = (chance - fact * 100 / times.toDouble()).absoluteValue

        assertTrue(deviation <= allowedDeviationPercent,
            "deviation more than allowed: $deviation > $allowedDeviationPercent")
    }

    @Test
    fun getTrueFailsToReturnAllFalse() {
        val times = 10_000
        val chance = 0

        val fact = repeat(times) { getTrue(chance) }
            .filter { it }
            .count()

        assertEquals(0, fact,
            "getTrue(0) shouldn't return true")
    }

    @Test
    fun getTrueFailsToReturnAllTrue() {
        val times = 10_000
        val chance = 100

        val fact = repeat(times) { getTrue(chance) }
            .filter { it }
            .count()

        assertEquals(times, fact,
            "getTrue(100) shouldn't return false")
    }

    @Test
    fun getTrueFailsToReturnOnePercentFalse() {
        val times = 10_000
        val chance = 99

        val foundFalse = repeat(times) { getTrue(chance) }
            .filter { !it }
            .any()

        assertTrue(foundFalse, "getTrue(99) should return at least one false")
    }

    @Test
    fun getTrueFailsToReturnOnePercentTrue() {
        val times = 10_000
        val chance = 1

        val foundFalse = repeat(times) { getTrue(chance) }
            .filter { it }
            .any()

        assertTrue(foundFalse, "getTrue(1) should return at least one true")
    }

    @Test
    fun getTrueFailsToReturnTrue() {
        assertTrue(getTrue(100))
    }

    /* Cover lines! */
    @Test
    fun coverDummyTest() {
        assertTrue(dummyFunction())
    }
}
