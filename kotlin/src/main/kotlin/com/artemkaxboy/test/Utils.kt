package com.artemkaxboy.test

import kotlin.random.Random

/**
 * Generates random boolean with given chance
 *
 * @param chance value in percent to get true
 * @return true if chance took place, false - otherwise
 */
fun getTrue(chance: Int): Boolean {
    return Random.nextDouble(1.0) < chance / 100.0
}

/**
 * Repeatedly executes given [block] code
 *
 * @param times amount of repetitions
 * @param block code to execute
 * @return sequence of results
 */
inline fun <T> repeat(times: Int, crossinline block: () -> T): Sequence<T> {
    return generateSequence { }.take(times)
        .map { block() }
}

fun dummyFunction(): Boolean {
    val sequence = sequenceOf("a", "b", "c", "d", "e", "f")
    sequence
        .onEach { println(it) }
        .map { it.first().toInt() }
        .onEach { println(it) }
        .map { it + Random.nextInt(10) }
        .onEach { println(it) }
        .filter { it % 2 == 0 }
        .forEach { println(it) }
    return getTrue(100)
}
